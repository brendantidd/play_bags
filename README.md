# Setup: #
Put all files into a `play_bags' folder in the home directory (e.g. /home/brendan/play_bags)

Change:
`launch_rviz.launch' : args="-d /home/brendan/play_bags/rviz_settings.rviz"
to:
args="-d /home/<your_home_directory_name>/play_bags/rviz_settings.rviz"

# Run: #
Run from commandline:
`bash rviz_and_bags.sh rate start path`

Where rate is playback speed in Hz, and art is the position you want to start

e.g: 
bash rviz_and_bags.sh 1 0 /home/path/to/bag/bag

## Remember to save the rviz settings!##
Add topics by name to visualise them, then file-> save -> /home/your_home/play_bags/rviz_settings.rviz. 

Also found this answer about a possible way  save from bag file to txt (might include timestamps):
https://answers.ros.org/question/9102/how-to-extract-data-from-bag/
