#!/bin/bash
# These are the command line arguments
RATE=$1
START=$2
BAGS=$3

# Launch rviz
sleep 1
roslaunch launch_rviz.launch &
echo "Launching RVIZ"

# Play the bag file
sleep 2
echo "Playing following bag files at $RATE Hz starting at $START seconds:"
echo "$BAGS"
rosbag play -r $RATE -s $START --clock $BAGS 

